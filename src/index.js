import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './form-render/index';
import reportWebVitals from './reportWebVitals';

const defaultValue = {
    schema: {
        type: 'object',
        properties: {},
    },
    displayType: 'row',
    showDescIcon: true,
    labelWidth: 120,
};

ReactDOM.render(
  <React.StrictMode>
    <App {...defaultValue}/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
